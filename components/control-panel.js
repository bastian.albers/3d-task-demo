import styled from 'styled-components';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  top: 0;
  right: 0;
  max-width: 280px;
  background: #fff;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.3);
  padding: 12px 24px;
  margin: 20px;
  font-size: 13px;
  line-height: 2;
  outline: none;
  z-index: 100;
`;

const propTypes = {
  children: PropTypes.node
};

export default class ControlPanel extends PureComponent {
  render() {
    return (
      <Container>
        {this.props.children}
      </Container>
    );
  }
}

ControlPanel.propTypes = propTypes;