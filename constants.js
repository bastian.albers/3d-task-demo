export const TERRAIN_IMAGE = 'http://0.0.0.0:5000/singleband/DGM1/{z}/{x}/{y}.png';
export const SURFACE_IMAGE = 'http://0.0.0.0:8000/cog/tiles/WebMercatorQuad/{z}/{x}/{y}.png?url=https://geodaten.niedersachsen.dev/Hannover_DOP20_Mitte.tif';
export const TILESET_URL = 'http://localhost/SceneServer/layers/0';

export const INITIAL_VIEW_STATE = {
    // Hannover Mitte
    latitude: 52.367202,
    longitude: 9.737389,
    zoom: 15,
    bearing: 0,
    pitch: 30,
    maxPitch: 85
};

export const ELEVATION_DECODER = {
    "rScaler": 1,
    "gScaler": 0,
    "bScaler": 0,
    "offset": 0
}
// export const ELEVATION_DECODER = {
//     "rScaler": 6553.6,
//     "gScaler": 25.6,
//     "bScaler": 0.1,
//     "offset": -10000
// }