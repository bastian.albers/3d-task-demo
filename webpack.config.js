const resolve = require('path').resolve;

const CONFIG = {
    mode: 'development',
    entry: {
        app: resolve('./app.js')
    },
    output: {
        library: 'App'
    },
    module: {
        rules: [
            {
                // Transpile ES6 to ES5 with babel
                // Remove if your app does not use JSX or you don't need to support old browsers
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: [/node_modules/],
                options: {
                    presets: ['@babel/preset-react']
                }
            }
        ]
    },
    devServer: {
        port: 8080,
        headers: {
            "Access-Control-Allow-Origin": "*"
        }
    }
};

module.exports = CONFIG;