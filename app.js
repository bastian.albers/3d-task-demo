/* global window, URL */
import React, {PureComponent} from 'react';
import {render} from 'react-dom';

import {lumaStats} from '@luma.gl/core';
import DeckGL from '@deck.gl/react';
import {MapController} from '@deck.gl/core';
import {Tile3DLayer} from '@deck.gl/geo-layers';
import {TerrainLayer} from '@deck.gl/geo-layers';
import {StatsWidget} from '@probe.gl/stats-widget';

import {I3SLoader} from '@loaders.gl/i3s';

import ControlPanel from './components/control-panel';
import {SURFACE_IMAGE, TERRAIN_IMAGE, TILESET_URL, INITIAL_VIEW_STATE, ELEVATION_DECODER} from './constants';

export default class App extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      viewState: INITIAL_VIEW_STATE,
      wireframe: false
    };
  }

  componentDidMount() {
    const container = this._statsWidgetContainer;
    this._memWidget = new StatsWidget(lumaStats.get('Memory Usage'), {
      framesPerUpdate: 1,
      formatters: {
        'GPU Memory': 'memory',
        'Buffer Memory': 'memory',
        'Renderbuffer Memory': 'memory',
        'Texture Memory': 'memory'
      },
      container
    });

    this._tilesetStatsWidget = new StatsWidget(null, {container});
  }

  _onWireframeChange() {
    this.setState({wireframe: !this.state.wireframe});
  }

  // Updates stats, called every frame
  _updateStatWidgets() {
    this._memWidget.update();
    this._tilesetStatsWidget.update();
  }

  // Called by DeckGL when user interacts with the map
  _onViewStateChange({viewState}) {
    this.setState({viewState});
  }

  _renderControlPanel() {

    const {viewState} = this.state;

    return (
      <ControlPanel>
        <div style={{textAlign: 'center'}}>
          long/lat: {viewState.longitude.toFixed(5)},{viewState.latitude.toFixed(5)},
          zoom:{' '} {viewState.zoom.toFixed(2)},
          bearing:{' '} {viewState.bearing.toFixed(0)},
          pitch:{' '} {viewState.pitch.toFixed(0)}
            <div>
                <label>wireframe 
                    <input 
                        type="checkbox"
                        defaultChecked={this.state.wireframe}
                        onChange={this._onWireframeChange.bind(this)}
                    />
                </label>
            </div>
        </div>
      </ControlPanel>
    );
  }

  _renderStats() {
    return (
      <div
        style={{
          position: 'absolute',
          padding: 12,
          zIndex: '10000',
          maxWidth: 300,
          background: '#000',
          color: '#fff'
        }}
        ref={_ => (this._statsWidgetContainer = _)}
      />
    );
  }

  _renderTile3DLayer() {
    return new Tile3DLayer({
        id: 'tile-3d-layer',
        data: TILESET_URL,
        loader: I3SLoader,
        // loadOptions,
        pickable: true
      });
  }

  _renderTerrainLayer(wireframeChecked) {
    return new TerrainLayer({
        id: 'terrain-layer',
        minZoom: 0,
        maxZoom: 23,
        strategy: 'no-overlap',
        elevationDecoder: ELEVATION_DECODER,
        elevationData: TERRAIN_IMAGE,
        texture: SURFACE_IMAGE,
        wireframe: wireframeChecked,
        color: [255, 255, 255]
      });
  }

  render() {
    const {viewState, wireframe} = this.state;
    const tile3DLayer = this._renderTile3DLayer();
    const terrainLayer = this._renderTerrainLayer(wireframe);

    return (
      <div style={{position: 'relative', height: '100%'}}>
        {this._renderStats()}
        {this._renderControlPanel()}
        <DeckGL
          layers={[tile3DLayer, terrainLayer]}
          viewState={viewState}
          onViewStateChange={this._onViewStateChange.bind(this)}
          controller={{type: MapController, maxPitch: 85}}
          onAfterRender={() => this._updateStatWidgets()}
        >
        </DeckGL>
      </div>
    );
  }
}

export function renderToDOM(container) {
  render(<App />, container);
}
