# Readme
Dies ist eines angepasste minimal Version des TerrainLayer Beispiels von der [deck.gl](http://deck.gl) Website.
Es wird Beispielhaft gezeigt, wie mit deck.gl 3D-Gebäudemodelle mit einem Geländemodell im Browser visualisiert werden.

## 1. Vorbereitung

Vor dem Ausführen dieser Demo müssen die drei Datensätze DGM, DOP und 3D Gebäudemodelle als Webdienste zur Verfügung stehen.

### Datengrundlagen

1. DGM1 Hannover Mitte: https://geodaten.niedersachsen.dev/Hannover_Mitte_DGM1.tif
2. DOP20 Hannover MItte: https://geodaten.niedersachsen.dev/Hannover_DOP20_Mitte.tif
3. 3D-Stadtmodell LOD2 Hannover: https://opengeodata.hannover-stadt.de/Stadtmodell_Hannover_CityGML_LoD2.zip

## 1.1 titiler
Das DOP20 wird mit dem XYZ Tile Server Titiler ausgeliefert.

```bash
docker run --name titiler \
    -d \
    -p 8000:8000 \
    --env PORT=8000 \
    --env WORKERS_PER_CORE=1 \
    --rm -it developmentseed/titiler
```

Das DOP20 Luftbild als COG kann nun mit folgender URL abgerufen werden

http://0.0.0.0:8000/cog/tiles/WebMercatorQuad/{z}/{x}/{y}.png?url=https://geodaten.niedersachsen.dev/Hannover_DOP20_Mitte.tif

**Nachteil Titiler:**
Titiler kann das DGM1 aktuell nicht verarbeiten, da die Pixel als Float32 abgespeichert sind:
```json
"PNG driver doesn't support data type Float32. Only eight bit (Byte) and sixteen bit (UInt16) bands supported."
```

**WORKAROUND:**
Das DGM1 COG zu RGB Werten mit diesem [Python-Script](https://gitlab.com/bastian.albers/convert_dgm_to_rgb) umrechnen, um möglichst exakte Höhenwerte beizubehalten.
Im [TerrainLayer von deck.gl](https://deck.gl/docs/api-reference/geo-layers/terrain-layer) muss dann nur noch ein Decoder für die Höhenwerte angegeben werden.
In diesem Fall müsste er dann so aussehen:
```javascript
{
  "rScaler": 6553.6,
  "gScaler": 25.6,
  "bScaler": 0.1,
  "offset": -10000
}
```

## 1.2 terracotta

Das DGM1 wird mit [terracotta](https://terracotta-python.readthedocs.io/en/latest/index.html), einem schlanken XYZ Tile Server auf Python-Basis, mit wenigen Kommandos und ohne umfangreicher Konfiguration zur Verfügung gestellt.
Das originale [COG](https://www.cogeo.org/) kann direkt eingebunden werden, eine weitere Verarbeitung ist nicht notwendig.

Anders als bei tititler muss für terracotta das [DGM1 COG](https://geodaten.niedersachsen.dev/Hannover_Mitte_DGM1.tif) lokal vorliegen.

Den *docker run* Befehl im Ordner ausführen, in dem Hannover_Mitte_DGM1.tif liegt:
```docker
docker run --rm -d -it -v /$(pwd)/:/data -p 5000:5000 bastianalbers/terracotta-docker
```

**Nachteil terracotta:**
terracotta ist auf singleband Satellitendaten spezialisiert. [Daher werden Multiband RGB Geotiffs nicht wirklich unterstützt](https://github.com/DHI-GRAS/terracotta/issues/125). Das Luftbild wird daher mit dem cog-tile-server ausgeliefert.

Das DGM1 ist danach beispielsweise unter folgender URL abrufbar: http://0.0.0.0:5000/singleband/DGM1/{z}/{x}/{y}.png

## 1.3 CityGML Dateien zu I3S konvertieren

Die o.g. [CityGML Dateien](https://opengeodata.hannover-stadt.de/Stadtmodell_Hannover_CityGML_LoD2.zip) müssen hierfür zunächst heruntergeladen und manuell mit der [FME Workbench "CityGML to I3S Toolbox"](https://www.arcgis.com/home/item.html?id=585b25bad7b94c09aa7fb1833db1f318) konvertiert werden.
Als Ergebnis erhält man eine I3S scene package layer (.slpk) Datei.

Da loaders.gl aktuell noch ein etwas angepasstes Format erwartet, wurde ein [Python Script](https://github.com/bastian-albers/fme_i3s_2_loadersgl_i3s) (und ein equivalentes [Bash Script](https://gitlab.com/holger.meuel/i3s-demonstrator/-/tree/master/convert-slpk-to-i3s-bashscript)) zur weiteren konvertierung entwickelt.

Das Python Script wird beispielsweise mit folgendem Befehl gestartet:
```bash
python3 fme_i3s_2_loadersgl_i3s.py -i Hannover.slpk
```

## 1.4 I3S-Server starten

Die 3D-Gebäudemodelle müssen im I3S-Format für den [i3s-server](https://loaders.gl/modules/tile-converter/docs/cli-reference/tile-converter#running-local-server-to-handle-i3s-layer) von loaders.gl vorliegen.

Sind die CityGML Daten korrekt konvertiert, kann der i3s-server gestartet werden.

Der folgende docker run Befehl muss den Pfad zum Ordner enthalten, in dem die mit dem Script konvertierten i3s Daten liegen.

```bash
docker run --rm -d -it -v HIER_PFAD_ANPASSEN:/app/data -p 80:80 bastianalbers/i3s-server
```
An einem konkreten Beispiel könnte es dann so aussehen:
```bash
docker run --rm -d -it -v /Users/bastianalbers/LOD2_Hannover_converted:/app/data -p 80:80 bastianalbers/i3s-server
```

Die i3s Daten können dann über diese URL abgerufen werden:

```
http://localhost/SceneServer/layers/0
```

## 1.5 Starten der Demo

```bash
# install dependencies
yarn
# bundle and serve the app with webpack
yarn start
```

## Weiterführende Links und Dokumentation
[Spawning-Area-Task #3D - Google-Docs](https://docs.google.com/presentation/d/1l8hCpkra9Rg-gcYFvw2OwDWpWT2VNODr1IQXNenxEn4/edit?ts=60227c0c#slide=id.gc03e57a517_1_115): Spawning-Area-Reviews, talk notes

[Spawning-Area-Task #3D - Hugo git](https://gitlab.com:lgln/stabsstelle/spawning-area/3dtaskhugo.git): talk notes, overview of Spawning area #3D task group and contact data

[cog-tile-server](https://github.com/atararaksin/cog-tile-server):
A fast on-the-fly tile server built with Geotrellis and akka-http. Serves XYZ tyles from a one-piece Cloud Optimized GeoTiff.

[terracotta website](https://terracotta-python.readthedocs.io/en/latest/index.html):

1. Use it as data exploration tool to quickly serve up a folder containing GeoTiff images with terracotta serve.

2. Make it your tile server backend on an existing webserver. You can ingest your data ahead of time (recommended) or on-demand.

3. Deploy it on serverless architectures such as AWS Lambda to serve tiles from S3 buckets. This allows you to build apps that scale almost infinitely with minimal maintenance!

[terracotta on github](https://github.com/DHI-GRAS/terracotta):
A light-weight, versatile XYZ tile server, built with Flask and Rasterio 🌍 

[terracotta on pypi.org](https://pypi.org/project/terracotta/):
Terracotta is a pure Python tile server that runs as a WSGI app on a dedicated webserver or as a serverless app on AWS Lambda. It is built on a modern Python 3.6 stack, powered by awesome open-source software such as Flask, Zappa, and Rasterio.

[deck.gl - Tile3DLayer Doku](https://deck.gl/docs/api-reference/geo-layers/tile-3d-layer):
The Tile3DLayer renders 3d tiles data formatted according to the 3D Tiles Specification and ESRI I3S , which is supported by the Tiles3DLoader.

[loaders.gl](https://loaders.gl/modules/tile-converter/docs/cli-reference/tile-converter#running-local-server-to-handle-i3s-layer):
Running local server to handle i3s layer.

[deck.gl - TerrainLayer Doku](https://deck.gl/docs/api-reference/geo-layers/terrain-layer):
The TerrainLayer reconstructs mesh surfaces from height map images, e.g. Mapzen Terrain Tiles, which encodes elevation into R,G,B values.

[deck.gl - TerrainLayer Example](https://github.com/visgl/deck.gl/tree/master/examples/website/terrain):
This is a minimal standalone version of the TerrainLayer example on deck.gl website.
